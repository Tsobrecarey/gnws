<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Article;
use \App\User;
use \App\Status;
use Auth;
use Session;

class ArticleController extends Controller
{
    public function index(){
    	$articles=Article::all();

    	return view('news', compact('articles'));
    }

    public function showaddarticle(){
    	return view('userviews.addarticle');
    }

    public function approval(){
    	$articles=Article::all();
    	// $users=User::all();

    	return view('adminviews.approval', compact('articles'));
    }

    public function addarticle(Request $reqst){
    	$new_article=new Article;
    	$new_article->title=$reqst->title;
    	$new_article->body=$reqst->body;
    	$new_article->status_id=3;
    	$new_article->user_id=Auth::user()->id;

    	$image=$reqst->file('imgpath');
    	$image_name=time().".".$image->getClientOriginalExtension();
    	$destination="images/";
    	$image->move($destination, $image_name);
    	$new_article->imgpath=$destination.$image_name;
    	$new_article->save();

    	Session::flash("message", "You have successfully submitted an article for review!");
    	return redirect('news');
    }

    public function article($id){
    	$article=Article::find($id);
    	return view('article', compact('article'));
    }

    public function review_article($id){
    	$article=Article::find($id);
    	return view('adminviews.review', compact('article'));
    }    

}
