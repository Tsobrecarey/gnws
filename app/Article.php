<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use \App\Status;
use \App\User;
use \App\Comment;

class Article extends Model
{
    public function status(){
    	return $this->belongsTo("\App\Status");
    }

    public function user(){
    	return $this->belongsTo("\App\User");
    }

    public function comment(){
    	return $this->belongsTo("\App\Comment");
    }
}
