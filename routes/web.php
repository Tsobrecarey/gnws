<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/news', 'ArticleController@index');

Route::get('/approval', 'ArticleController@approval');

Route::get('/addarticle', 'ArticleController@showaddarticle');

Route::post('/addarticle', 'ArticleController@addarticle');

Route::get('/article/{id}', 'ArticleController@article');

Route::get('/review_article/{id}', 'ArticleController@review_article');