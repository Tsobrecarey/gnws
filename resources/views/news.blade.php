@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">All News</h1>

@foreach($articles as $article)
	@if($article->status_id == 1)
		<div class="col-lg-6 offset-lg-2"></div>
			<div class="card">
				<div class="card-body d-flex justify-content-center align-items-center">
					<img src="{{asset($article->imgpath)}}" alt="News Image" style="width:20%;height: 100%">
					<a href="/article/{{$article->id}}" style="width: 80%; font-size: 1.75rem">{{$article->title}}</a>
				</div>
			</div>
		</div>
	@else
	@endif
@endforeach

@endsection