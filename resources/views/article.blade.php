@extends('layouts.app')
@section('content')

<h1 class="text-center">{{$article->title}}</h1>
{{-- <h6 class="text-center">{{$article->user_id->name}}</h6> --}}

<div class="col-lg-10 offset-lg-1">
	<div class="text-center p-5">
	<img src="{{asset($article->imgpath)}}" style="height: 40vh">
	</div>
	<p class="text-justify" style="text-indent: 5%">{{$article->body}}</p>
</div>

@endsection