@extends('layouts.app')
@section('content')

<h1 class="text-center">All Articles for Approval</h1>

<div class="col-lg-10 offset-lg-1">
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Body</th>
				<th>Writer</th>
				<th>Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($articles as $article)
				<tr>
					<td style="max-width: 150px; text-overflow: ellipsis; white-space: nowrap;
						overflow: hidden;">{{$article->title}}</td>
					<td style="max-width: 300px; text-overflow: ellipsis; white-space: nowrap;
						overflow: hidden;">{{$article->body}}</td>
					<td>{{$article->user->name}}</td>
					<td>{{$article->status->name}}</td>
					<td>
						<a href="/review_article/{{$article->id}}" class="btn btn-success">Review</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection