@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Submit Article</h1>
<div class="col-lg-10 offset-lg-1">
	<form action="/addarticle" method="POST" class="form-group mx-3" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control">
		</div>
		<div class="form-group">
			<label for="body">Body</label>
			<textarea name="body" class="form-control" style="height: 20vw"></textarea>
		</div>
		<div class="form-group">
			<label for="imgpath">Image</label>
			<input type="file" name="imgpath" class="form-control">
		</div>
		<button class="btn btn-info" type="submit">Submit Article</button>
	</form>
</div>
@endsection